package com.example.backend.respositories;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;


// @Data
// @Builder
// @NoArgsConstructor
// @AllArgsConstructor
// @JsonIgnoreProperties(ignoreUnknown = true)

@Entity
@Table(name = "application", schema = "public")
public class Application {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "case_id")
    private String caseId;

    @Column(name = "fname")
    private String fname;

    @Column(name = "lname")
    private String lname;

    @Column(name = "mname")
    private String mname;

    @Column(name = "dob")
    private String dob;

    @Column(name = "ssn")
    private String ssn;

    @Column(name = "case_status")
    private String caseStatus;

    public Application() {}

    public Application(String caseId, String fname, String lname, String mname, String dob, String ssn, String caseStatus) {
        super();
        this.caseId = caseId;
        this.fname = fname;
        this.lname = lname;
        this.mname = mname;
        this.dob = dob;
        this.ssn = ssn;
        this.caseStatus = caseStatus;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public void setCaseStatus(String caseStatus) {
        this.caseStatus = caseStatus;
    }   
    
    public String getCaseId() {
        return this.caseId;
    }

    public String getFname() {
        return this.fname;
    }

    public String getLname() {
        return this.lname;
    }

    public String getMname() {
        return this.mname;
    }

    public String getDob() {
        return this.dob;
    }

    public String getSsn() {
        return this.ssn;
    }

    public String getCaseStatus() {
        return this.caseStatus;
    }   
}
