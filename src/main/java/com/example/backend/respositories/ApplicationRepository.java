package com.example.backend.respositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ApplicationRepository extends CrudRepository<Application, String> {
}
