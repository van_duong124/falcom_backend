package com.example.backend.respositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, String> {
    List<Person> findByFnameAndLnameAndMnameAndDobAllIgnoreCase(String fname, String lname, String mname, String dob);
}
