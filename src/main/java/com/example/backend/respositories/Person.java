package com.example.backend.respositories;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)

@Entity
@Table(name = "person", schema = "public")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person_id")
    private String personId;

    @Column(name = "fname")
    private String fname;

    @Column(name = "lname")
    private String lname;

    @Column(name = "mname")
    private String mname;

    @Column(name = "gender")
    private String gender;

    @Column(name = "dob")
    private String dob;

    @Column(name = "passport")
    private String passport;

    @Column(name = "ssn")
    private String ssn;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "city_of_residence")
    private String cityOfResidence;

    @Column(name = "state")
    private String state;

    @Column(name = "zip")
    private String zip;

    // public Person() {}

    // public Person(String personId, String fname, String lname, String mname, String gender, String dob, String passport, String ssn, String streetAddress, String cityOfResidence, String state, String zip) {
    //     super();
    //     this.personId = personId;
    //     this.fname = fname;
    //     this.lname = lname;
    //     this.mname = mname;
    //     this.gender = gender;
    //     this.dob = dob;
    //     this.passport = passport;
    //     this.ssn = ssn;
    //     this.streetAddress = streetAddress;
    //     this.cityOfResidence = cityOfResidence;
    //     this.state = state;
    //     this.zip = zip;
    // }

    // public void setPersonId(String personId) {
    //     this.personId = personId;
    // }

    // public void setFname(String fname) {
    //     this.fname = fname;
    // }

    // public void setLname(String lname) {
    //     this.lname = lname;
    // }

    // public void setMname(String mname) {
    //     this.mname = mname;
    // }

    // public void setGender(String gender) {
    //     this.gender = gender;
    // }

    // public void setDob(String dob) {
    //     this.dob = dob;
    // }

    // public void setPassport(String passport) {
    //     this.passport = passport;
    // }

    // public void setSsn(String ssn) {
    //     this.ssn = ssn;
    // }

    // public void setStreetAddress(String streetAddress) {
    //     this.streetAddress = streetAddress;
    // }

    // public void setCityOfResidence(String cityOfResidence) {
    //     this.cityOfResidence = cityOfResidence;
    // }

    // public void setState(String state) {
    //     this.state = state;
    // }

    // public void setZip(String zip) {
    //     this.zip = zip;
    // }   
    
    // public String getPersonId() {
    //     return this.personId;
    // }

    // public String getFname() {
    //     return this.fname;
    // }

    // public String getLname() {
    //     return this.lname;
    // }

    // public String getMname() {
    //     return this.mname;
    // }

    // public String setGender() {
    //     return this.gender;
    // }

    // public String getDob() {
    //     return this.dob;
    // }

    // public String getPassport() {
    //     return this.passport;
    // }

    // public String getSsn() {
    //     return this.ssn;
    // }

    // public String getStreetAddress() {
    //     return this.streetAddress;
    // }

    // public String getCityOfResidence() {
    //     return this.cityOfResidence;
    // }

    // public String getState() {
    //     return this.state;
    // }

    // public String getZip() {
    //     return this.zip;
    // }   
}
