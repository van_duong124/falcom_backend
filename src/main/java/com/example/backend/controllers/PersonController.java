package com.example.backend.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.respositories.Person;
import com.example.backend.respositories.PersonRepository;

@RestController
@RequestMapping("/dhs")
public class PersonController {

    private final PersonRepository personRepository; 
    
    PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping(path = "/person")
    public List<Person> GetPerson(String fname, String lname, String mname, String dob) {
        return this.personRepository.findByFnameAndLnameAndMnameAndDobAllIgnoreCase(fname, lname, mname, dob);
    }

    @PostMapping("/person")
    public Person updatePerson(@RequestBody Person person) {
        return this.personRepository.save(person);
    }

    @GetMapping(path = "/persons")
    public Iterable<Person> GetAllPersons() {
        return this
            .personRepository
            .findAll();
    }
}
