package com.example.backend.controllers;

import java.util.Optional;
import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.respositories.Application;
import com.example.backend.respositories.ApplicationRepository;

@RestController
@RequestMapping("/verify")
public class ApplicationController {

    private final ApplicationRepository applicationRepository; 
    
    ApplicationController(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @PostMapping(path = "/application/new")
    public Application createApplication(@RequestBody Application application) {
        application.setCaseId(UUID.randomUUID().toString());
        return this.applicationRepository.save(application);
    }

    @GetMapping(path = "/application")
    public Optional<Application> GetApplicationByCaseId(String caseId) {
        return this.applicationRepository.findById(caseId);
    }


    @PostMapping(path = "/application/review")
    public Application ReviewApplicationByCaseId(@RequestBody ApplicationReview applicationReview) {
        Optional<Application> application = this.applicationRepository.findById(applicationReview.getCaseId());

        if(application.isPresent()) {
            application.get().setCaseStatus(applicationReview.getCaseStatus());
            return this.applicationRepository.save(application.get());
        }
        return null;
    }
}
