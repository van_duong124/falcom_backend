CREATE TABLE IF NOT EXISTS "person" (
    person_id VARCHAR(255) PRIMARY KEY,
    
    fname VARCHAR(255), 
    lname VARCHAR(255),
    mname VARCHAR(255),

    gender VARCHAR(255), 
    dob VARCHAR(255), 

    passport VARCHAR(255),
    ssn VARCHAR(255),

    street_address VARCHAR(255), 
    city_of_residence VARCHAR(255), 
    state VARCHAR(255), 
    zip VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS "application" (
    case_id VARCHAR(255) PRIMARY KEY,
    
    fname VARCHAR(255), 
    lname VARCHAR(255),
    mname VARCHAR(255),

    dob VARCHAR(255), 
    ssn VARCHAR(255),

    case_status VARCHAR(255),

    CHECK (case_status IN ('reviewing', 'employment_authorized', 'employment_not_authorized', 'record_mismatch', 'close_case_and_resubmit'))
);

INSERT INTO person(person_id, fname, lname, mname, gender, dob, passport, ssn, street_address, city_of_residence, state, zip)
VALUES
('1', 'Arisha', 'Barron', 'NMN', 'M', '1992-01-02', 'YH82378U47', '123456789', '418 Court Street', 'De Soto', 'Missouri', '63020'),
('2', 'Rhonda', 'Church', 'NMN', 'M', '1994-01-02', 'YH82378U45', '123456789', '1810 Everette Alley', 'De Soto', 'Missouri', '63020'),
('3', 'Branden', 'Gibson', 'NMN', 'M', '1993-01-02', 'YH82378U46', '123456789', '418 Court Street', 'Fort Lauderdale', 'Florida', '33301'),
('4', 'Georgina', 'Hazel', 'NMN', 'M', '1995-01-02', 'YH82378U44', '123456789', '1844 Hillhaven Drive Suite M321', 'Burbank', 'California', '91502');

INSERT INTO application(case_id, fname, lname, mname, dob, ssn, case_status)
VALUES
('123', 'Arisha', 'Barron', 'NMN', '1992-01-02', '123456789', 'reviewing');